// Description:
//   Announce pull requests to the channel
//
// Dependencies:
//   "request-promise": "4.2.4"
//
// Configuration:
//   GITHUB_TOKEN
//   POLL_INTERVAL
//   USER_AGENT
//
// Author:
//   philbooth

'use strict'

const request = require('request-promise')

const {
  GITHUB_TOKEN,
  POLL_INTERVAL,
  USER_AGENT,
} = process.env

const ACTIONS = new Set([
  'review_requested',
  'ready_for_review',
  'opened',
  'reopened',
])
const EVENTS_URI = 'https://api.github.com/repos/mozilla/fxa/events'

module.exports = robot => {
  if (robot.adapterName === 'irc') {
    // The redis brain doesn't work in Slack, making this script too spammy there
    poll()
  }

  async function poll () {
    let pollInterval = POLL_INTERVAL || 60000

    try {
      const headers = {
        Authorization: `token ${GITHUB_TOKEN}`,
        'User-Agent': USER_AGENT || 'philbooth',
      }
      const etag = robot.brain.get('etag')
      if (etag) {
        headers['If-None-Match'] = etag
      }

      robot.logger.info(`polling ${EVENTS_URI}, headers=${JSON.stringify(headers, null, '  ')}`)
      robot.logger.debug(`PRs in brain: ${robot.brain.get('prs')}`)

      const prs = JSON.parse(robot.brain.get('prs') || '{}')

      const response = await request({
        uri: EVENTS_URI,
        headers,
        resolveWithFullResponse: true,
        simple: false,
      })

      if (response.statusCode === 200) {
        robot.logger.debug(`GitHub response headers: ${response.headers}`)

        if (response.headers.etag) {
          robot.brain.set('etag', response.headers.etag)
        }

        if (response.headers['x-poll-interval']) {
          pollInterval = parseInt(response.headers['x-poll-interval']) * 1000
        }

        const events = JSON.parse(response.body)
        if (Array.isArray(events)) {
          events.forEach(event => {
            const { action, number, pull_request: pr } = event.payload
            const key = `${number}-${action}`
            if (event.type === 'PullRequestEvent' && ACTIONS.has(action) && ! prs[key]) {
              prs[key] = true

              let room = 'fxa'
              let message = `PR #${number}: ${action} by ${pr.user.login}\n${pr.html_url}`
              if (robot.adapterName === 'irc') {
                room = `#${room}`
                message = `${message}\n${pr.title}`
              }

              robot.messageRoom(room, message)
            }
          })

          robot.brain.set('prs', JSON.stringify(prs))
        } else {
          robot.logger.error('GitHub returned non-array response body')
        }
      } else {
        robot.logger.warning(`ignoring GitHub ${response.statusCode} response`)
      }
    } catch (error) {
      robot.logger.error(error.stack)
    }

    setTimeout(poll, pollInterval)
  }
}

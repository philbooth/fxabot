// Description:
//   Nominate FxA developers for tasks on a fair, round-robin basis
//
// Dependencies:
//   "check-types": "8.0.2"
//
// Commands:
//   hubot nominate - Fairly nominate an FxA developer to fulfil some obligation
//
// Author:
//   philbooth

'use strict'

const assert = require('assert')
const check = require('check-types')

const IRC_DEVS = [ 'pb', 'stomlinson', 'vbudhram', 'dcoates', 'ianbicking', 'lorchard' ]
const SLACK_DEVS = [ '@pbooth', '@stomlinson', '@vbudhram', '@dcoates', '@Ian', '@lorchard' ]

assert.equal(IRC_DEVS.length, SLACK_DEVS.length)

module.exports = robot => {
  robot.respond(/nominate(?:\s+dev(?:eloper)?)?\s*$/i, response => {
    robot.logger.debug(`respond: ${response.match[0]}`)

    let lastDev = robot.brain.get('dev')
    if (check.not.greaterOrEqual(lastDev, 0) || check.not.integer(lastDev)) {
      robot.logger.debug('No dev in brain')
      lastDev = -1
    }

    const nextDev = (lastDev + 1) % IRC_DEVS.length
    robot.brain.set('dev', nextDev)
    const devs = robot.adapterName === 'irc' ? IRC_DEVS : SLACK_DEVS
    response.send(`I nominate ${devs[nextDev]}`)
  })
}

// Description:
//   Commands for reporting FxA train information.
//
// Dependencies:
//   "fxhey": "0.2.3"
//   "moment": "2.24.0"
//   "mongodb": "3.2.3"
//   "vague-time": 2.4.2"
//
// Configuration:
//   DB_USER
//   DB_PASSWORD
//   DB_SERVER
//   DB_PORT
//   DB_NAME
//
// Commands:
//   fxhey - Report the currently-deployed FxA train information
//   fxhey auth - Report the currently-deployed FxA auth server information
//   fxhey content - Report the currently-deployed FxA content server information
//   fxhey oauth - Report the currently-deployed FxA OAuth server information
//   fxhey profile - Report the currently-deployed FxA profile server information
//   fxhey when was <train> - Report the deployment time for <train>
//   fxhey when was auth <version> - Report the deployment time for auth server <version>
//   fxhey when was content <version> - Report the deployment time for content server <version>
//   fxhey when was oauth <version> - Report the deployment time for OAuth server <version>
//   fxhey when was profile <version> - Report the deployment time for profile server <version>
//   fxhey what happened on <date-time> - Report the deployment info either side of <date-time>
//
// Notes:
//   Connects to the same mongodb instance used for https://philbooth.me/fxa
//
// Author:
//   philbooth

'use strict'

const fxhey = require('fxhey')
const moment = require('moment')
const { MongoClient } = require('mongodb')
const vagueTime = require('vague-time')

const { DB_USER, DB_PASSWORD, DB_SERVER, DB_PORT, DB_NAME } = process.env
const DB_URI = `mongodb://${DB_USER}:${DB_PASSWORD}@${DB_SERVER}:${DB_PORT}`

const CACHED_DATA = {
  history: {},
  status: {}
}

const SERVERS = [
  {
    name: 'auth',
    baseUri: 'https://api.accounts.firefox.com/',
    attributes: [ 'commit' ]
  },
  {
    name: 'content',
    baseUri: 'https://accounts.firefox.com/',
    attributes: [ 'commit', 'l10n' ]
  },
  {
    name: 'oauth',
    baseUri: 'https://oauth.accounts.firefox.com/',
    attributes: [ 'commit' ]
  },
  {
    name: 'profile',
    baseUri: 'https://profile.accounts.firefox.com/',
    attributes: [ 'commit' ]
  }
]

const SERVER_NAMES = new Set([ 'auth', 'content', 'oauth', 'profile' ])

module.exports = robot => {
  setImmediate(registerForAnnouncements)

  robot.hear(/^\s*fxhey\s*!?\s*$/i, async response => {
    try {
      robot.logger.debug(`hear: ${response.match[0]}`)

      const status = await getCachedData('status')
      const ago = vagueTime.get({ to: status.timeObject })

      response.send(`FxA train ${status.train}, updated ${ago} (${status.time})`)
    } catch (error) {
      robot.logger.error(error.stack)
      response.send('I need to lie down')
    }
  })

  SERVERS.forEach(server => {
    const { name } = server

    robot.respond(new RegExp(`${name}\\s*$`, 'i'), response => {
      robot.logger.debug(`respond: ${response.match[0]}`)

      robot.http(`${server.baseUri}/__version__`)
        .get()((error, httpResponse, body) => {
          try {
            if (error) {
              robot.logger.error(error.stack)
              return response.send(error.message)
            }

            const status = JSON.parse(body)
            const attributes = server.attributes.map(attribute => `${attribute}=${status[attribute]}`)

            response.send(`FxA ${name} server ${status.version}, ${attributes.join(', ')}`)
          } catch (err) {
            robot.logger.error(err.stack)
            response.send('I\'m not feeling great')
          }
        })
    })
  })

  robot.respond(/when\s+was\s+([a-z]+\s+)?v?([0-9.]+)\s*$/i, async response => {
    try {
      robot.logger.debug(`respond: ${response.match[0]}`)

      let name = response.match[1]
      if (name) {
        name = name.trim()
        if (! SERVER_NAMES.has(name)) {
          robot.logger.error('Couldn\'t parse server name')
          return response.send('Don\'t be silly')
        }
      } else {
        name = 'fxa'
      }

      const { train, patch } = parseVersion(response.match[2])

      if (! train) {
        robot.logger.error('Couldn\'t parse version')
        return response.send('You\'re not making much sense')
      }

      const history = await getCachedData('history')

      let match
      history.some(deployment => deployment.some(item => {
        if (
          item.train === train &&
          item.name === name && (
            item.patch === patch ||
            name === 'fxa'
          )
        ) {
          match = item
          return true
        }

        return false
      }))

      if (! match) {
        robot.logger.error('Couldn\'t find version')
        return response.send(`Sorry, I don't have ${name} ${train}.${patch || 0} in my history`)
      }

      const time = new Date(match.time)
      const ago = vagueTime.get({ to: time })
      const when = marshallFriendlyTime(time)

      if (name === 'fxa') {
        name = 'FxA'
      } else {
        name = `FxA ${name} server`
      }

      return response.send(`${name} train ${train}${patch >= 0 ? `, patch ${patch}` : ''}: ${ago} (${when})`)
    } catch (error) {
      robot.logger.error(error.stack)
      response.send('I don\'t understand')
    }
  })

  robot.respond(/what\s+(?:happened|deployed|shipped|went\s+live)\s+(?:(?:on|around|near)\s+)?([a-z0-9\s/-]+)\s*$/i, async response => {
    try {
      robot.logger.debug(`respond: ${response.match[0]}`)

      const whenString = response.match[1].trim()
      const when = moment(whenString)
      const history = await getCachedData('history')

      let after, before
      const found = history.some(deployment => {
        const { time } = deployment[0]
        if (time > when.toDate().getTime()) {
          after = deployment.filter(item => item.time === time)
        } else {
          before = deployment.filter(item => item.time === time)
          return true
        }

        return false
      })

      if (! found) {
        return response.send(`${whenString} was before my time, sorry`)
      }

      response.send(marshallHistoryItems(before))

      if (after) {
        return response.send(marshallHistoryItems(after))
      }

      response.send('And that\'s your lot')
    } catch (error) {
      robot.logger.error(error.stack)
      response.send('You what mate?')
    }
  })

  function registerForAnnouncements () {
    robot.logger.debug('registerForAnnouncements')

    let collection, mongo

    return connect()
      .then(result => {
        mongo = result
        collection = mongo.db(DB_NAME).collection('versions')

        return collection.find({}).toArray()
      })
      .then(versions => {
        robot.logger.info(`found ${versions.length} versions in the database`, versions)

        let initialStatus

        if (versions.length > 0) {
          initialStatus = versions.reduce((status, version) => {
            if (version.name === 'fxa') {
              status.train = version.train
              status.time = version.time
            } else {
              status.versions.push(version)
            }

            return status
          }, { versions: [] })
        }

        robot.logger.info('registering for FxA announcements')

        fxhey(announceStatus, {
          rate: 1000 * 60 * 30,
          status: initialStatus
        })
      })
      .catch(error => robot.logger.error(error.stack))
      .then(() => {
        if (mongo) {
          return disconnect(mongo)
        }
      })
  }

  function announceStatus (error, status) {
    robot.logger.debug('announceStatus')

    if (error) {
      return robot.logger.error(error.stack)
    }

    robot.logger.info('received FxA status', {
      train: status.train,
      time: status.time
    })

    status.diffs.forEach(diff => {
      const { name, current, previous } = diff
      robot.messageRoom(`FxA ${name} server has been deployed! Train ${current.train}, patch ${current.patch} (changed from ${previous.train}.${previous.patch}).`)
    })

    setCachedData('status', marshallStatus(status))

    let collections, mongo

    return connect()
      .then(result => {
        mongo = result
        const db = mongo.db(DB_NAME)
        collections = [ db.collection('versions'), db.collection('oldVersions') ]

        return Promise.all(collections.map(collection => collection.find({}).toArray()))
      })
      .then(([ currentVersions, oldVersions ]) => {
        oldVersions = unpackOldVersions(oldVersions)

        if (! hasOldVersion(oldVersions, currentVersions)) {
          oldVersions.unshift(currentVersions)
        }

        setCachedData('history', oldVersions)
      })
      .catch(err => robot.logger.error(err.stack))
      .then(() => {
        if (mongo) {
          return disconnect(mongo)
        }
      })
  }
}

function getCachedData (key) {
  const cache = CACHED_DATA[key]

  if (cache.item) {
    return Promise.resolve(cache.item)
  }

  return new Promise(resolve => {
    cache.resolve = resolve
  })
}

function parseVersion (version) {
  const split = version.split('.')

  if (split.length === 1) {
    return {
      train: parseInt(split[0])
    }
  }

  if (split.length === 2) {
    return {
      train: parseInt(split[0]),
      patch: parseInt(split[1])
    }
  }

  return {
    train: parseInt(split[1]),
    patch: parseInt(split[2])
  }
}

function connect () {
  const mongo = new MongoClient(DB_URI, {
    authSource: DB_NAME,
    j: 1,
    useNewUrlParser: true,
    w: 1,
    wtimeout: 10000
  })
  return mongo.connect()
    .then(() => mongo)
}

function disconnect (mongo) {
  return mongo.close()
}

function marshallStatus (status) {
  const time = new Date(status.time)
  return {
    servers: status.versions.map(version => {
      const serverTime = version.time ? new Date(version.time) : time
      return {
        name: marshallName(version.name),
        slug: `${version.name}-server`,
        train: version.train,
        patch: version.patch,
        repo: version.repo,
        tag: version.tag,
        commit: version.commit,
        commitShort: version.commit.substr(0, 7),
        time: marshallFriendlyTime(serverTime),
        timeObject: serverTime
      }
    }),
    train: status.train,
    time: marshallFriendlyTime(time),
    timeObject: time
  }
}

function marshallName (name) {
  if (name === 'oauth') {
    return 'OAuth'
  }

  return `${name[0].toUpperCase()}${name.substr(1)}`
}

function marshallFriendlyTime (t) {
  return `${pad(t.getUTCDate())}-${month(t.getUTCMonth())}-${t.getUTCFullYear()} ${pad(t.getUTCHours())}:${pad(t.getUTCMinutes())} UTC`
}

function pad (number) {
  if (number >= 0 && number < 10) {
    return `0${number}`
  }

  return number
}

function month (number) {
  switch (number) {
    case 0:
      return 'Jan'
    case 1:
      return 'Feb'
    case 2:
      return 'Mar'
    case 3:
      return 'Apr'
    case 4:
      return 'May'
    case 5:
      return 'Jun'
    case 6:
      return 'Jul'
    case 7:
      return 'Aug'
    case 8:
      return 'Sep'
    case 9:
      return 'Oct'
    case 10:
      return 'Nov'
    case 11:
      return 'Dec'
    default:
      throw new Error(`Bad month ${number}`)
  }
}

function setCachedData (key, data) {
  const cache = CACHED_DATA[key]

  cache.item = data

  if (cache.resolve) {
    cache.resolve(cache.item)
    delete cache.resolve
  }
}

function unpackOldVersions (oldVersions) {
  switch (oldVersions.length) {
    case 0:
      return oldVersions

    case 1:
      return oldVersions[0].array

    default:
      throw new Error(`Database contains ${oldVersions.length} old versions.`)
  }
}

function hasOldVersion (oldVersions, currentVersions) {
  if (currentVersions.length === 0) {
    return true
  }

  if (oldVersions.length === 0) {
    return false
  }

  return findMainVersion(oldVersions[0]).time >= findMainVersion(currentVersions).time
}

function findMainVersion (versions) {
  let result

  versions.some(version => {
    if (version.name === 'fxa') {
      result = version
      return true
    }

    return false
  })

  return result
}

function marshallHistoryItems (deployment) {
  const time = deployment[0].time

  const servers = deployment
    .filter((item, index) => index !== 0 && item.time === time)
    .map(item => `${item.name} server ${item.tag}, commit ${item.commit}`)

  return `${marshallFriendlyTime(new Date(time))}: ${servers.join('; ')}`
}

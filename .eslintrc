extends:
  - eslint:recommended

parserOptions:
  ecmaVersion: 9

env:
  es6: true
  node: true

rules:
  accessor-pairs: 2
  array-bracket-spacing: [ 2, "always" ]
  arrow-spacing: 2
  block-scoped-var: 1
  block-spacing: 2
  brace-style: 2
  callback-return: 2
  camelcase: 2
  class-methods-use-this: 2
  comma-spacing: 2
  comma-style: 2
  computed-property-spacing: 2
  consistent-this: [ 2, "self" ]
  curly: [ 2, "all" ]
  dot-location: [ 2, "property" ]
  dot-notation: 2
  eol-last: 2
  eqeqeq: 2
  func-call-spacing: 2
  func-name-matching: 2
  func-names: [ 2, "never" ]
  func-style: [ 2, "declaration" ]
  generator-star-spacing: 2
  getter-return: 2
  guard-for-in: 2
  handle-callback-err: [ 2, "^err" ]
  indent: [ 2, 2, { "SwitchCase": 1 } ]
  jsx-quotes: 2
  key-spacing: 2
  keyword-spacing: 2
  line-comment-position: 2
  linebreak-style: 2
  lines-between-class-members: 2
  max-depth: [ 1, 3 ]
  max-len: [ 1, { "code": 100, "tabWidth": 2, "ignoreRegExpLiterals": true, "ignoreStrings": true, "ignoreTemplateLiterals": true, "ignoreUrls": true } ]
  max-nested-callbacks: [ 1, 6 ]
  max-statements-per-line: [ 1, { "max": 1 } ]
  multiline-comment-style: [ 2, "separate-lines" ]
  new-cap: 2
  new-parens: 2
  newline-per-chained-call: 2
  no-alert: 2
  no-array-constructor: 2
  no-bitwise: 2
  no-buffer-constructor: 2
  no-caller: 2
  no-catch-shadow: 2
  no-console: 0
  no-div-regex: 1
  no-duplicate-imports: 2
  no-else-return: 2
  no-empty-pattern: 2
  no-eq-null: 2
  no-eval: 2
  no-extend-native: 2
  no-extra-bind: 2
  no-extra-label: 2
  no-fallthrough: 0
  no-floating-decimal: 2
  no-implied-eval: 2
  no-invalid-this: 2
  no-iterator: 2
  no-label-var: 2
  no-labels: 2
  no-lone-blocks: 2
  no-lonely-if: 2
  no-loop-func: 2
  no-multi-spaces: 2
  no-multi-str: 2
  no-multiple-empty-lines: [ 2, { "max": 1, "maxEOF": 1, "maxBOF": 0 } ]
  no-negated-condition: 2
  no-nested-ternary: 2
  no-new: 2
  no-new-object: 2
  no-new-func: 2
  no-new-require: 2
  no-new-wrappers: 2
  no-octal-escape: 2
  no-path-concat: 2
  no-proto: 2
  no-prototype-builtins: 2
  no-redeclare: [ 2, { "builtinGlobals": true } ]
  no-restricted-globals: 2
  no-return-await: 2
  no-script-url: 2
  no-self-compare: 2
  no-sequences: 2
  no-shadow: [ 2, { "hoist": "all" } ]
  no-shadow-restricted-names: 2
  no-tabs: 2
  no-template-curly-in-string: 1
  no-throw-literal: 1
  no-trailing-spaces: 2
  no-undef-init: 2
  no-undefined: 2
  no-underscore-dangle: 2
  no-unmodified-loop-condition: 2
  no-unneeded-ternary: 2
  no-unused-expressions: 2
  no-use-before-define: [ 2, { "functions": false } ]
  no-useless-call: 2
  no-useless-computed-key: 2
  no-useless-concat: 2
  no-useless-constructor: 2
  no-useless-rename: 2
  no-var: 2
  no-void: 2
  no-whitespace-before-property: 2
  no-with: 2
  object-curly-newline: 2
  object-curly-spacing: [ 2, "always" ]
  object-shorthand: 2
  operator-assignment: 2
  operator-linebreak: [ 2, "after" ]
  padded-blocks: [ 2, "never" ]
  prefer-arrow-callback: 2
  prefer-const: 2
  prefer-numeric-literals: 2
  prefer-promise-reject-errors: 1
  prefer-rest-params: 2
  prefer-spread: 2
  prefer-template: 2
  quote-props: [ 2, "as-needed" ]
  quotes: [ 2, "single", { "avoidEscape": true } ]
  require-await: 2
  rest-spread-spacing: 2
  semi: [ 2, "never" ]
  sort-imports: 2
  space-before-blocks: 2
  space-before-function-paren: 2
  space-in-parens: 2
  space-infix-ops: 2
  space-unary-ops: [ 2, { "words": true, "nonwords": false, "overrides": { "!": true, "!!": true } } ]
  strict: 2
  switch-colon-spacing: 2
  symbol-description: 2
  template-curly-spacing: 2
  template-tag-spacing: 2
  unicode-bom: 2
  valid-jsdoc: 2
  wrap-iife: 2
  yield-star-spacing: [ 2, "before" ]
  yoda: 2
